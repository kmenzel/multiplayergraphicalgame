package graphicgame

class MutableLL[A](defaultVal: A) {
  case class Node(val data: A, var next: Node)
  var head: Node = null
  var len = 0

  def add(data: A) = {
    if (head == null)
      head = new Node(data, null)
    else {
      var rover = head
      while (rover.next != null)
        rover = rover.next
      rover.next = new Node(data, null)
    }
    len += 1
  }

  def removeSpot(index: Int): A = {
    if (index == 0) {
      val ret = head.data
      head = head.next
      len -= 1
      ret
    } else {
      var currentNode = head
      for (i <- 0 until index - 1)
        currentNode = currentNode.next
      val ret = currentNode.next.data
      currentNode.next = currentNode.next.next
      len -= 1
      ret
    }
  }

  def apply(index: Int): A = {
    var currentNode = head
    for (i <- 0 until index)
      currentNode = currentNode.next
    currentNode.data
  }

  def isEmpty: Boolean = head == null
  def size: Int = len

}