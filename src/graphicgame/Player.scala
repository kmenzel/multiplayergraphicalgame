package graphicgame

class Player(level: Level, val name: String, initX: Int, initY: Int) extends Character(initX, initY) {
  private var mScore = 0
  private var up = false
  private var down = false
  private var left = false
  private var right = false

  override def update(): Unit = {
    //println(s"Player $name is at x:${super.x} y:${super.y}")
    val layout = level.layout
    if (up) {
      if (super.y > 1 && !level.isOccupied(super.x, super.y-1)) {
        move(0, -1)
      }
    }
    if (down) {
      if (super.y < layout.length - 2 && !level.isOccupied(super.x, super.y+1)) {
        move(0, 1)
      }
    }
    if (left) {
      if (super.x > 1 && !level.isOccupied(super.x-1, super.y)) {
        move(-1, 0)
      }
    }
    if (right) {
      if (super.x < layout(0).length - 2 && !level.isOccupied(super.x+1, super.y)) {
        move(1, 0)
      }
    }
  }

  def upPressed(): Unit = up = true
  def downPressed(): Unit = down = true
  def leftPressed(): Unit = left = true
  def rightPressed(): Unit = right = true

  def upReleased(): Unit = up = false
  def downReleased(): Unit = down = false
  def leftReleased(): Unit = left = false
  def rightReleased(): Unit = right = false
  
  def increseScore():Unit = mScore += 1
  def score():Int = mScore
}