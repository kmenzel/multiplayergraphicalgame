package graphicgame

import scala.collection.mutable

class Level(val layout: Array[Array[Boolean]]) {
  val players = new MutableLL[Player](null)
  val playerLocs = new mutable.ListBuffer[(Int, Int)]
  val enemies = new MutableLL[Enemy](null)
  val enemyLocs = new mutable.ListBuffer[(Int, Int)]
  val projs = new MutableLL[Projectile](null)
  val projLocs = new mutable.ListBuffer[(Double, Double)]

  def isOccupied(x: Int, y: Int): Boolean = {
    (layout(y)(x) || playerLocs.contains(x -> y) || enemyLocs.contains(x -> y))
  }

  def isOccupiedNonPlayer(x: Int, y: Int): Boolean = {
    (layout(y)(x) || enemyLocs.contains(x -> y))
  }

  def isEnemy(x: Int, y: Int): Boolean = {
    enemyLocs.contains(x -> y)
  }

  //Count is used to slow down the speed of the enemies
  def update(count: Int): Unit = {
    for (i <- 0 until players.size) {
      players(i).update()
      playerLocs.update(i, players(i).x -> players(i).y)
    }

    if (count % 15 == 0) {
      for (i <- 0 until enemies.size) {
        enemies(i).update()
        enemyLocs.update(i, enemies(i).x -> enemies(i).y)
      }
    }

    //Because this loop can remove projectiles, if one does get removed then it will keep looping until one after the last element and crash
    var i = 0
    while (i < projs.size) {
      if (projs(i).update())
        projLocs.update(i, projs(i).x -> projs(i).y)
      i += 1
    }
  }

  def shortestPath(x1: Int, y1: Int, x2: Int, y2: Int): List[(Int, Int)] = {
    val queue = new mutable.Queue[(Int, Int)]()
    var pathRecord: List[(Int, Int)] = List[(Int, Int)]()
    var visited = List[(Int, Int)]()
    queue.enqueue(x1 -> y1)
    while (!queue.isEmpty) {
      val (x, y) = queue.dequeue()
      //if (!visited.contains((x, y))) {
        //visited ::= x -> y
        pathRecord ::= x -> y
        if (x == x2 && y == y2)
          return pathRecord

        val maxWidth = layout(0).length
        val maxHeight = layout.length

        if (x - 1 >= 0 && x - 1 < maxWidth && !isOccupied(x - 1, y) && !visited.contains(x-1, y))
          queue.enqueue(x - 1 -> y)

        if (x + 1 >= 0 && x + 1 < maxWidth && !isOccupied(x + 1, y) && !visited.contains(x+1, y))
          queue.enqueue(x + 1 -> y)

        if (y - 1 >= 0 && y - 1 < maxHeight && !isOccupied(x, y - 1) && !visited.contains(x, y-1))
          queue.enqueue(x -> (y - 1))

        if (y + 1 >= 0 && y + 1 < maxHeight && !isOccupied(x , y + 1) && !visited.contains(x, y+1))
          queue.enqueue(x -> (y + 1))
          
        visited ::= x -> y
      //}
    }

    pathRecord
  }

  def getElement(x: Int, y: Int): Boolean = layout(x)(y)

  def getPassable(): PassableLevel = {
    new PassableLevel(layout, playerLocs, enemyLocs, projLocs)
  }

  def addPlayer(p: Player) = {
    players.add(p)
    playerLocs += p.x -> p.y
  }

  def removePlayer(p: Player) = {
    for (i <- 0 until players.size)
      if (players(i) == p) {
        players.removeSpot(i)
        playerLocs.remove(i)
      }
  }

  def addEnemy(e: Enemy) = {
    enemies.add(e)
    enemyLocs += e.x -> e.y
  }

  def removeEnemy(e: Enemy) = {
    for (i <- 0 until enemies.size)
      if (enemies(i) == e) {
        enemies.removeSpot(i)
        enemyLocs.remove(i)
      }
  }

  def removeEnemy(x: Int, y: Int) = {
    for (i <- 0 until enemyLocs.size)
      if (enemyLocs(0) == x -> y) {
        enemies.removeSpot(i)
        enemyLocs.remove(i)
      }
  }

  def addProj(p: Projectile) = {
    projs.add(p)
    projLocs += p.x -> p.y
  }

  def removeProj(p: Projectile) = {
    var i = 0
    while (i < projs.size) {
      if (projs(i) == p) {
        projs.removeSpot(i)
        projLocs.remove(i)
      }
      i += 1
    }
  }
}