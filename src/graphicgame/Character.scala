package graphicgame

abstract class Character(initX:Int, initY:Int) {
  private var mx:Int = initX
  private var my:Int = initY
  
  def x:Int = mx
  def y:Int = my
  
  def move(dx:Int, dy:Int):Unit = {
    mx += dx
    my += dy
  }
  
  def update():Unit
}