package graphicgame

import java.rmi.Remote
import java.rmi.server.UnicastRemoteObject
import java.rmi.Naming
import java.rmi.registry.LocateRegistry
import scala.collection.mutable
import scalafx.application.Platform
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/* Dr Lewis,
 * This program has been experiencing many issues with multithreading and ScalaFX.
 * Upon launching a client and entering a name, an exception will sometimes be throws.
 * I am still working on a solution, but for grading purposes, if it crashes try running
 * it a few more times. One of those times will usually get the client up and working
 * properly. 
 * 
 * After that I'm not telling where any of the other bugs(features) are.
 * Consider it like your own personal easter egg hung :)
 * 
 * -Karl
 */

@remote trait GameServer extends Remote {
  def connect(name: String, ref: GameClient): Unit
  def getScoreboard(): String
  def click(name: String, clickX: Double, clickY: Double, sceneWidth: Int, sceneHeight: Int): Unit
  def upPressed(name: String): Unit
  def downPressed(name: String): Unit
  def leftPressed(name: String): Unit
  def rightPressed(name: String): Unit
  def upReleased(name: String): Unit
  def downReleased(name: String): Unit
  def leftReleased(name: String): Unit
  def rightReleased(name: String): Unit
}

object ServerMain extends UnicastRemoteObject with GameServer {
  val fals = false //This is so each value is only 4 long and the map is easier to read
  val level = new Level(Array(Array(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true),
    Array(true, fals, fals, true, fals, fals, fals, fals, fals, fals, fals, fals, fals, fals, true),
    Array(true, true, fals, true, fals, true, fals, true, fals, true, true, true, true, fals, true),
    Array(true, true, fals, true, fals, true, fals, true, fals, fals, fals, true, fals, true, true),
    Array(true, true, fals, true, fals, true, fals, true, true, true, fals, true, fals, true, true),
    Array(true, fals, fals, fals, fals, fals, fals, true, fals, fals, fals, true, fals, true, true),
    Array(true, fals, true, fals, true, fals, true, true, fals, true, true, fals, fals, fals, true),
    Array(true, fals, true, fals, true, fals, fals, fals, fals, fals, fals, true, fals, true, true),
    Array(true, fals, true, fals, true, true, fals, true, fals, true, fals, true, fals, true, true),
    Array(true, fals, fals, true, fals, fals, fals, true, true, fals, fals, true, fals, true, true),
    Array(true, true, fals, true, fals, true, fals, true, fals, fals, true, fals, fals, fals, true),
    Array(true, fals, fals, fals, fals, true, fals, true, fals, true, fals, true, fals, true, true),
    Array(true, fals, true, true, true, fals, fals, true, fals, true, fals, true, fals, true, true),
    Array(true, fals, fals, fals, fals, fals, fals, true, fals, fals, fals, fals, fals, fals, true),
    Array(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true)))
  val users = mutable.Buffer[(String, GameClient)]()
  val players = mutable.Map[String, Player]()
  val SCOREBOARD_SIZE = 10
  val scoreBoard: Array[Player] = Array.fill(SCOREBOARD_SIZE)(null)

  def main(args: Array[String]): Unit = {
    LocateRegistry.createRegistry(1099)
    Naming.rebind("ServerMain", this)
    level.addEnemy(new Enemy(12, 3, level))
    level.addEnemy(new Enemy(11, 6, level))
    
    var count = 0
    while (true) {
      count += 1
      level.update(count)
      for (u <- users) {
        println("Running")
        u._2.update(level.getPassable())
      }
      Thread.sleep(100)
    }
  }

  def updateScoreboard(loc: Int, players: mutable.Map[String, Player]): Unit = {
    if (loc == SCOREBOARD_SIZE)
      return
    else {
      var highestPlayer: Player = null
      for (p <- players) {
        if (highestPlayer == null || p._2.score() > highestPlayer.score())
          highestPlayer = p._2
      }
      scoreBoard(loc) = highestPlayer
      updateScoreboard(loc + 1, players.filterNot(_._2 == highestPlayer))
    }
  }

  def getScoreboard(): String = {
    updateScoreboard(0, players)
    var ret = ""
    for (p <- 0 until scoreBoard.length) {
      if (scoreBoard(p) != null)
        ret += s"${p + 1}. ${scoreBoard(p).name}: ${scoreBoard(p).score()}\n"
    }
    ret
  }

  def connect(name: String, ref: GameClient): Unit = {
    users += name -> ref
    players += name -> new Player(level, name, 1, 1)
    ref.update(level.getPassable())
    level.addPlayer(players(name))
  }

  def click(name: String, clickX: Double, clickY: Double, sceneWidth: Int, sceneHeight: Int): Unit = {
    val squareWidth = sceneWidth.toDouble / level.layout(0).length
    val squareHeight = sceneHeight.toDouble / level.layout.length
    val pX = (players(name).x * squareWidth) + (squareWidth / 2)
    val pY = (players(name).y * squareHeight) + (squareHeight / 2)
    val oppisite = clickY - pY
    val adjacent = clickX - pX
    val theta = Math.atan2(oppisite, adjacent)

    //This actually represents the hypotenuse of the triangle, which is how far the projectile moves in each update cycle
    //The higher the number the less distance traveled in each cycle (smoother but slower)
    val projSpeed = .3 //Lower than .25 gets too jumpy

    level.addProj(new Projectile(level, pX, pY, Math.cos(theta) / projSpeed, Math.sin(theta) / projSpeed, sceneWidth, sceneHeight, players(name)))
  }

  def upPressed(name: String): Unit = players(name).upPressed()
  def downPressed(name: String): Unit = players(name).downPressed()
  def leftPressed(name: String): Unit = players(name).leftPressed()
  def rightPressed(name: String): Unit = players(name).rightPressed()

  def upReleased(name: String): Unit = players(name).upReleased()
  def downReleased(name: String): Unit = players(name).downReleased()
  def leftReleased(name: String): Unit = players(name).leftReleased()
  def rightReleased(name: String): Unit = players(name).rightReleased()
}