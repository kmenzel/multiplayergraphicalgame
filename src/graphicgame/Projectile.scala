package graphicgame

class Projectile(level: Level, initX: Double, initY: Double, val dx: Double, val dy: Double, sceneWidth:Int, sceneHeight:Int, player:Player) {
  private var mx: Double = initX
  private var my: Double = initY

  def x: Double = mx
  def y: Double = my

  def move(): Unit = {
    mx += dx
    my += dy
  }

  //Returns true if this projectile can stay in the game
  def update(): Boolean = {
    move()
    val xCord = (mx / (sceneWidth/level.layout(0).length)).toInt
    val yCord = (my / (sceneHeight/level.layout.length)).toInt
    //println(s"mx:$mx my:$my x:$xCord y:$yCord")
    if (level.isOccupiedNonPlayer(xCord, yCord)) {
      if (level.isEnemy(xCord, yCord)){
        player.increseScore()
        level.removeEnemy(xCord, yCord)
      }
      level.removeProj(this)
      return false
    } else
      true
  }
}