package graphicgame

import java.rmi.Remote
import java.rmi.server.UnicastRemoteObject
import java.rmi.Naming
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.control.TextInputDialog
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scalafx.scene.input.KeyEvent
import scalafx.scene.input.KeyCode
import scalafx.Includes._

@remote trait GameClient extends Remote {
  def update(level: PassableLevel): Unit
}

object ClientMain extends UnicastRemoteObject with GameClient {
  val sceneWidth = 700
  val sceneHeight = 700
  val GAME_NAME = "Karl's Boring Shooter"
  val renderer = new Renderer(sceneWidth, sceneHeight)

  def update(level: PassableLevel): Unit = {
    renderer.render(level)
  }

  def main(args: Array[String]): Unit = {
    val server = Naming.lookup("rmi://localhost/ServerMain") match {
      case s: GameServer => s
      case _ => throw new RuntimeException("No RMI connection established, server not found!")
    }

    val app = new GameApp(this, server, renderer, GAME_NAME, sceneWidth, sceneHeight)
    app.main(args)
  }
}