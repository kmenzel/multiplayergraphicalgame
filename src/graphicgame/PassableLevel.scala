package graphicgame

import scala.collection.mutable

class PassableLevel(val layout:Array[Array[Boolean]], val playerLocs:mutable.ListBuffer[(Int, Int)], val enemyLocs:mutable.ListBuffer[(Int, Int)], val projLocs:mutable.ListBuffer[(Double, Double)]) extends Serializable {
  def getElem(x:Int, y:Int):Boolean = layout(x)(y)
}