package graphicgame

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint.Color

class Renderer(sceneWidth: Int, sceneHeight: Int) {
  var gc: GraphicsContext = null
  def render(level: PassableLevel) = {
    if (gc != null) {
      val layout = level.layout
      val squareWidth = sceneWidth.toDouble / layout(0).length
      val squareHeight = sceneHeight.toDouble / layout.length
      for (row <- 0 until layout.length)
        for (col <- 0 until layout(row).length) {
          gc.fill = if (layout(row)(col)) Color.Black else Color.White
          gc.fillRect(col * squareWidth, row * squareHeight, squareWidth, squareHeight)
        }

      val players = level.playerLocs
      gc.fill = Color.Blue
      for (i <- 0 until players.size)
        gc.fillRect(players(i)._1 * squareWidth, players(i)._2 * squareHeight, squareWidth, squareHeight)

      val enemies = level.enemyLocs
      gc.fill = Color.Red
      for (i <- 0 until enemies.size)
        gc.fillRect(enemies(i)._1 * squareWidth, enemies(i)._2 * squareHeight, squareWidth, squareHeight)
        
      val projs = level.projLocs
      gc.fill = Color.Black
      for(i <- 0 until projs.size)
        gc.fillRect(projs(i)._1, projs(i)._2, 5, 5)
        
    } else
      throw new NullPointerException("The GraphicsContext is null")
  }

  def setGraphicsContext(newGC: GraphicsContext) = gc = newGC
}