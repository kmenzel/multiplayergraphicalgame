package graphicgame

class Enemy(initX: Int, initY: Int, level: Level) extends Character(initX, initY) {
  var currentPath = List[(Int, Int)]()
  
  //THE SHORTEST PATH IS CURRENTLY UNDER CONSTRUCTION
  override def update(): Unit = {
    //Every enemy only goes for the first player
    if(currentPath.length == 0 && level.players.size > 0)
      currentPath = level.shortestPath(x, y, level.players(0).x, level.players(0).y).reverse
    if (level.players.size > 0 && currentPath(currentPath.length-1) != level.players(0).x -> level.players(0).y)
      currentPath = level.shortestPath(x, y, level.players(0).x, level.players(0).y).reverse
      //println(level.shortestPath(x, y, level.players(0).x, level.players(0).y).reverse)
      
    if (currentPath.size > 0) {
      if (currentPath(0) == (x -> y))
        currentPath = currentPath.drop(1)

      if (currentPath.size > 0) {
        val xDiff = currentPath(0)._1-x
        val yDiff = currentPath(0)._2-y
        move(xDiff, yDiff)
      }
    }
  }
}