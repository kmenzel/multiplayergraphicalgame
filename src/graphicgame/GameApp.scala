package graphicgame

import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.control.TextInputDialog
import scalafx.scene.canvas.Canvas
import scalafx.scene.input.KeyCode
import scalafx.scene.input.KeyEvent
import scalafx.Includes._
import scalafx.scene.input.MouseEvent
import scalafx.scene.control.MenuBar
import scalafx.scene.control.Menu
import scalafx.scene.control.MenuItem
import scalafx.event.ActionEvent
import scalafx.scene.control.Alert
import scalafx.scene.control.Alert.AlertType

class GameApp(val client: GameClient, val server: GameServer, val renderer: Renderer, GAME_NAME: String, sceneWidth: Int, sceneHeight: Int) extends JFXApp {
  stage = new JFXApp.PrimaryStage {
    title = GAME_NAME
    scene = new Scene(sceneWidth, sceneHeight) {

      val menuBar = new MenuBar
      menuBar.prefWidth = sceneWidth
      val scoreMenu = new Menu("Score")
      val scoreItem = new MenuItem("Scoreboard")
      scoreMenu.items = List(scoreItem)
      menuBar.menus = List(scoreMenu)
      
      val canvas = new Canvas(sceneWidth, sceneHeight)
      val gc = canvas.graphicsContext2D
      renderer.setGraphicsContext(gc)

      val nameInputBox = new TextInputDialog("John Doe") {
        initOwner(null)
        title = GAME_NAME
        headerText = "Welcome to " + GAME_NAME
        contentText = "Please enter your name: "
      }
      val data = nameInputBox.showAndWait()
      val name = data match {
        case Some(tempName) => {
          tempName.trim()
        }
        case None => {
          System.exit(0)
          ""
        }
      }

      server.connect(name, client)

      content = List(canvas, menuBar)

      scoreItem.onAction = (e:ActionEvent) => {
        new Alert(AlertType.Information){
          initOwner(stage)
          title = "Highscore Board"
          headerText = "High Scores:"
          contentText = server.getScoreboard()
        }.showAndWait()
      }
      
      canvas.onKeyPressed = (e: KeyEvent) => {
        e.code match {
          case KeyCode.Up => server.upPressed(name)
          case KeyCode.Down => server.downPressed(name)
          case KeyCode.Left => server.leftPressed(name)
          case KeyCode.Right => server.rightPressed(name)
          case _ =>
        }
      }

      canvas.onKeyReleased = (e: KeyEvent) => {
        e.code match {
          case KeyCode.Up => server.upReleased(name)
          case KeyCode.Down => server.downReleased(name)
          case KeyCode.Left => server.leftReleased(name)
          case KeyCode.Right => server.rightReleased(name)
          case _ =>
        }
      }

      canvas.onMouseClicked = (e: MouseEvent) => {
        server.click(name, e.x, e.y, sceneWidth, sceneHeight)
      }

      canvas.requestFocus()

    }
  }
}